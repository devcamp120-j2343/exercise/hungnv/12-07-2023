//Import thu vien express
const express = require("express");
//Khai bao middleware
const companyMiddleware = require("../middleware/companyMiddleware");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
// Middleware - Get all companies
router.get('/companies', companyMiddleware.getAllCompanyMiddleware, (req, res) => {
    class Company {
        constructor(id, company, contact, country) {
            this.id = id;
            this.company = company;
            this.contact = contact;
            this.country = country;
        }
    }

    const companies = [
        new Company(1, 'Alfreds Futterkiste', 'Maria Anders', 'Germany'),
        new Company(2, 'Centro comercial Moctezuma', 'Francisco Chang', 'Mexico'),
        new Company(3, 'Ernst Handel', 'Roland Mendel', 'Austria'),
        new Company(4, 'Island Trading', 'Helen Bennett', 'UK'),
        new Company(5, 'Laughing Bacchus Winecellars', 'Yoshi Tannamuri', 'Canada'),
        new Company(6, 'Magazzini Alimentari Riuniti', 'Giovanni Rovelli', 'Italy')
    ];

    res.json(companies);
});
// Middleware - Get a company by ID
router.get('/companies/:id', companyMiddleware.getACompanyMiddleware, (req, res) => {
    const id = req.params.id; // Get company ID from request params

    // Logic to fetch the company with the specified ID from the database

    res.json({ id, company: 'Company Name', contact: 'Contact Name', country: 'Country' });
});
// Middleware - Create a new company
router.post('/companies',companyMiddleware.createACompanyMiddleware, (req, res) => {
    const { company, contact, country } = req.body; // Get company details from request body
  
    // Logic to create a new company in the database
  
    res.json({ message: 'Company created successfully' });
  });
  
  // Middleware - Update a company by ID
  router.put('/companies/:id',companyMiddleware.updateACompanyMiddleware, (req, res) => {
    const id = req.params.id; // Get company ID from request params
    const { company, contact, country } = req.body; // Get updated company details from request body
  
    // Logic to update the company with the specified ID in the database
  
    res.json({ message: 'Company updated successfully' });
  });
  
  // Middleware - Delete a company by ID
  router.delete('/companies/:id',companyMiddleware.deleteACompanyMiddleware, (req, res) => {
    const id = req.params.id; // Get company ID from request params
  
    // Logic to delete the company with the specified ID from the database
  
    res.json({ message: 'Company deleted successfully' });
  });
  
  module.exports = router;