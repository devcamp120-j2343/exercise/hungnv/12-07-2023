const getAllCompanyMiddleware = (req, res, next) => {
    console.log("Get All Company!");
    next();
}
const getACompanyMiddleware = (req, res, next) => {
    console.log("Get a Company!");
    next();
}
const createACompanyMiddleware = (req, res, next) => {
    console.log("Create a Company!");
    next();
}
const updateACompanyMiddleware = (req, res, next) => {
    console.log("Update a Company!");
    next();
}
const deleteACompanyMiddleware = (req, res, next) => {
    console.log("Delete a Company!");
    next();
}

//export
module.exports={
    getAllCompanyMiddleware,
    getACompanyMiddleware,
    createACompanyMiddleware,
    updateACompanyMiddleware,
    deleteACompanyMiddleware
}