//Import thu vien ExpressJS
//import express form 'express'
const express = require('express');

//khai bao router
const companyRouter = require('./app/router/companyRouter');
//Khoi tao app node.js bang express
const app = express();

//Khai bao ung dung se chay cong 8000
const port = 8000;

app.use("/api/company", companyRouter);
//Chay ung dung tren cong port:8000
app.listen(port, () => {
    console.log("Ung dung chay tren cong : ", port)
});